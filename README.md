# Hult Developer Test

## Getting Started

1. Fork this repository.
2. Clone your new repository.
3. Commit/branch/push your code as you would normally do.
4. When finished, replace the readme file with a small description on how to build and run the project.

## What we expect from you

This task involves build up a web application to match the mobile designs included in the repository, using any frontend technologies of your choosing.
The solution should take into account:

- This is a web application which would be access by different devices/browsers.
- The solution should show an understanding of the problems involved and make use of the tech stack to solve them.
- We'll be givin more emphasys on code quality (Reliability, Reusability, Maintainability, ...).
- We're not testing your Frontend Design skills, but, try to be true to the designs as much as you can.

## A little guidance

This is a shareable report, the final page of an application.
The user has built their career path, by selecting roles he desires to follow, and the skills he aready pocess for each role.

We're displaying the missing skills, for each role, and directing the user to appropiate programs.

You'll find a mock api call, with the User Report Data, in the data folder.

You can use any library/framework you feel confortable with.

- Used font - "Helvetica"
- For the icons use the following from https://fontawesome.com/ :
  - user
  - briefcase
  - graduation-cap
  - clock
  - flag
  - dollar-sign
  - chart-line
  - fire

## Design Overview

![Design overview](/designs/CareerMapperReport.png?raw=true "Design overview")
