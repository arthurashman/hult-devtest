function ProfileModel({firstName, lastName, currentRole, education, experience}) {
  this._firstName = firstName;
  this._lastName = lastName;
  this._currentRole = currentRole;
  this._education = education;
  this._experience = experience;
}

ProfileModel.prototype = {
  getFirstName: function() {
    return this._firstName;
  },

  getLastName: function() {
    return this._lastName;
  },

  getCurrentRole: function() {
    return this._currentRole;
  },

  getEducation: function() {
    return this._education;
  },

  getExperience: function() {
    return this._experience;
  }

}