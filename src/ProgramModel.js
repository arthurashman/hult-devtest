function ProgramModel({title, description, coveredSkills, link}) {
  this._title = title;
  this._description = description;
  this._coveredSkills = coveredSkills;
  this._link = link;
}

ProgramModel.prototype = {
  getTitle: function() {
    return this._title;
  },

  getDescription: function() {
    return this._description;
  },

  getCoveredSkills: function() {
    return this._coveredSkills;
  },

  getLink: function() {
    return this._link;
  }
}