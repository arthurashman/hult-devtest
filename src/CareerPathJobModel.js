function CareerPathJobModel({jobName, meanSalary, demandLevel, experienceYears, inDemandSkills}) {
  this._jobName = jobName;
  this._meanSalary = meanSalary;
  this._demandLevel = demandLevel;
  this._experienceYears = experienceYears;
  this._inDemandSkills = inDemandSkills;
}

CareerPathJobModel.prototype = {
  getJobName: function() {
    return this._jobName;
  },

  getMeanSalary: function() {
    return this._meanSalary;
  },

  getDemandLevel: function() {
    return this._demandLevel;
  },

  getExperienceYears: function() {
    return this._experienceYears;
  },

  getInDemandSkills: function() {
    return this._inDemandSkills;
  }
}
