describe("CareerPathJobModel", function() {

  var careerPathJobModel;

  beforeEach(function() {
    careerPathJobModel = new CareerPathJobModel({
      jobName: "Senior Employee",
      meanSalary: "60000",
      demandLevel: "High",
      experienceYears: "5",
      inDemandSkills: [
        {"name": "Skill 1", "description": "Skill 1"},
        {"name": "Skill 2", "description": "Skill 2"},
        {"name": "Skill 3", "description": "Skill 3"}
      ]
    })
  })
  
  describe("#getJobName", function() {
    it("can return job name", function() {
      expect(careerPathJobModel.getJobName()).toEqual("Senior Employee")
    })
  })

  describe("#getMeanSalary", function() {
    it("can return mean salary", function() {
      expect(careerPathJobModel.getMeanSalary()).toEqual("60000")
    })
  })

  describe("#getDemandLevel", function() {
    it("can return demand level", function() {
      expect(careerPathJobModel.getDemandLevel()).toEqual("High")
    })
  })

  describe("#getExperienceYears", function() {
    it("can return experience years", function() {
      expect(careerPathJobModel.getExperienceYears()).toEqual("5")
    })
  })

  describe("#getInDemandSkills", function() {
    it("can return in demand skills", function() {
      expect(careerPathJobModel.getInDemandSkills()).toEqual([
        {"name": "Skill 1", "description": "Skill 1"},
        {"name": "Skill 2", "description": "Skill 2"},
        {"name": "Skill 3", "description": "Skill 3"},
      ])
    })
  })
});
