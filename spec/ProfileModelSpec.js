describe("profileModel", function() {

  var profileModel;

  beforeEach(function() {
    profileModel = new ProfileModel({
      firstName: "John",
      lastName: "Doe",
      currentRole: "Employee",
      education: "Degree",
      experience: "1 year"})
  })
  
  describe("#getFirstName", function() {
    it("can return first name", function() {
      expect(profileModel.getFirstName()).toEqual("John")
    })
  })

  describe("#getLastName", function() {
    it("can return first name", function() {
      expect(profileModel.getLastName()).toEqual("Doe")
    })
  })

  describe("#getCurrentRole", function() {
    it("can return current role", function() {
      expect(profileModel.getCurrentRole()).toEqual("Employee")
    })
  })
  
  describe("#getEducation", function() {
    it("can return education", function() {
      expect(profileModel.getEducation()).toEqual("Degree")
    })
  })

  describe("#getExperience", function() {
    it("can return experience", function() {
      expect(profileModel.getExperience()).toEqual("1 year")
    })
  })
});
