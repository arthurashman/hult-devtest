describe("ProgramModel", function() {

  var programModel;

  beforeEach(function() {
    programModel = new ProgramModel({
      title: "MBA",
      description: "An MBA is great",
      coveredSkills: [
        {"name": "Skill 1", "Id": 1},
        {"name": "Skill 2", "Id": 2},
        {"name": "Skill 3", "Id": 3}
      ],
      link: "http://www.hult.edu/en/mba/one-year-mba/",
    })
  })

  describe("#getTitle", function() {
    it("can return title", function() {
      expect(programModel.getTitle()).toEqual("MBA")
    })
  })

  describe("#getDescription", function() {
    it("can return description", function() {
      expect(programModel.getDescription()).toEqual("An MBA is great")
    })
  })

  describe("#getCoveredSkills", function() {
    it("can return covered skills", function() {
      expect(programModel.getCoveredSkills()).toEqual([
        {"name": "Skill 1", "Id": 1},
        {"name": "Skill 2", "Id": 2},
        {"name": "Skill 3", "Id": 3}
      ])
    })
  })

  describe("#getLink", function() {
    it("can return link", function() {
      expect(programModel.getLink()).toEqual("http://www.hult.edu/en/mba/one-year-mba/")
    })
  })
})